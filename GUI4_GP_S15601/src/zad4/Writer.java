/**
 *
 *  @author Głąb Piotr S15601
 *
 */

package zad4;

public class Writer implements Runnable {

	public Author autor;
	public static volatile boolean isAuthorWorking = true;

	public Writer(Author autor) {
		this.autor = autor;
	}

	@Override
	public void run() {
		while (isAuthorWorking || !autor.abc.isEmpty())
			try {
				Thread.sleep(1000);
				System.out.println(autor.abc.take());
			} catch (InterruptedException exc) { }
	}
}
