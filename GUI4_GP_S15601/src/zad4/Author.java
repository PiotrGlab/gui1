/**
 *
 *  @author Głąb Piotr S15601
 *
 */

package zad4;

import java.util.concurrent.ArrayBlockingQueue;

public class Author implements Runnable {

	public ArrayBlockingQueue<String> abc;
	public String[] txt;

	public Author(String[] args) {
		this.txt = args;
		abc = new ArrayBlockingQueue<>(txt.length);
	}

	@Override
	public void run() {
		for (int i = 0; i < txt.length; i++) {
			try {
				Thread.sleep(1000);
				abc.put(txt[i]);
			} catch (InterruptedException exc) {
			}
		}
		Writer.isAuthorWorking = false;
	}
}
