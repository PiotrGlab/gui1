package zad2;

public class StringTask implements Runnable{
	
	private String input;
	private int count;
	private String currentResult = "";
	private TaskState currentState = TaskState.CREATED;
	private Thread executionThread;

	public StringTask(String input, int count) {
		this.input = input;
		this.count = count;
	}
	
	public String getResult() {
		return currentResult;
	}
	
	public TaskState getState(){
		return currentState;
	}
	
	public void start() {
		executionThread = new Thread(this);
		executionThread.start();
	}
	
	public void abort() {
		currentState = TaskState.ABORTED;
	}
	
	public boolean isDone() {
		return (currentState == TaskState.READY || currentState == TaskState.ABORTED);
	}
	
	@Override
	public void run() {
		currentState = TaskState.RUNNING;
		for (int i = 0; i < count; i++) {
			if(currentState == TaskState.ABORTED)
				return;
			currentResult += input;
		}
		currentState = TaskState.READY;
	}
}
