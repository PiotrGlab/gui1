package zad1;

import java.util.ArrayList;

public class Letters {
	
	private String input;
	private ArrayList<Printer> result;
	
	public Letters(String input) {
		this.input = input;
	}

	public ArrayList<Printer> getThreads() {
		if(result == null) {
			result = new ArrayList<>();
			for(int i = 0; i < input.length(); i++){
				String tmp = input.substring(i, i+1);
				result.add(new Printer(tmp));
			}
		}
		return result;
	}

}
