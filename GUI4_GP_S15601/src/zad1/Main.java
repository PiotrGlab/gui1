/**
 *
 *  @author Głąb Piotr S15601
 *
 */

package zad1;


public class Main {

  public static void main(String[] args) throws InterruptedException {
    Letters letters = new Letters("ABCD");
    for (Thread t : letters.getThreads()) System.out.println(t.getName());
    
	for (Thread t : letters.getThreads()) t.start();
	

    Thread.sleep(5000);
   
    for (Printer t : letters.getThreads()) {
    	while (t.isInterrupted() == false) {
    		t.interrupt();
    	}
    }
//    Printer.terminate();
//    for (Printer t : letters.getThreads()) {
//    	while(t.ifRunning() == false);
//    }
    
    System.out.println("\nProgram skończył działanie");
  }

}
