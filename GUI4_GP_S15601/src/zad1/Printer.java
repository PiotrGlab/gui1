package zad1;

public class Printer extends Thread{
	
	private String output;
	private static volatile boolean running = true;
	private volatile boolean isRunning = true;
	
	public Printer(String output) {
		super("Thread " + output);
		this.output = output;
	}
	
	public static void terminate() {
		running = false;
	}
	
	public boolean ifRunning(){
		if (isRunning == false)
			return true;
		return false;
	}
	
	public void run() {
		while(running){
			try {
				Thread.sleep(100);
			} catch (InterruptedException iex) {
				return;
			}
			System.out.print(output);
		}
		isRunning = false;
	}
}
