/**
 *
 *  @author Głąb Piotr S15601
 *
 */

package zad2;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Finder {
	private String wholeFile;
	private String path;

	public Finder(String fname){
		path = fname;
		try{
			wholeFile = new String(Files.readAllBytes(Paths.get(path)));
		}catch(Exception ex){
			System.out.println("Brak pliku lub niewlasciwe dane");
		}
	}
	
	public int getIfCount() {
		wholeFile = " " + wholeFile;
		String wholeFileWithoutCommentsOneline = wholeFile.replaceAll("\\/\\/.*", " ");
		String wholeFileWithoutCommentsOthers = wholeFileWithoutCommentsOneline.replaceAll("[\\/\\*].*[\\*\\/]", " ");
		String wholeFileWithoutStrings = wholeFileWithoutCommentsOthers.replaceAll("[\"].*[\"]", " ");
		String wholeFileWithoutWhitespace = wholeFileWithoutStrings.replaceAll("\\s+", " ");
		String[] wholeFileSeparated = wholeFileWithoutWhitespace.split("(\\s|[;]|[)]|[}]|[{])(if\\s*[(])");
		int sum = wholeFileSeparated.length - 1;
		return sum;

	}
	
	public int getStringCount(String string) {
		int sum = 0;
		String wholeFileWithoutString = wholeFile.replace(string, "");
		int difference = wholeFile.length() - wholeFileWithoutString.length();
		sum = difference / string.length();
		
		return sum;
	}
	
}    
