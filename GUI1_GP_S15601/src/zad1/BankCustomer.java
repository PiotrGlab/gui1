package zad1;

public class BankCustomer {
	
	private Account account;
	private Person person;
	
	public BankCustomer(Person person) {
		this.person = person;
		account = new Account();
	}
	
	public Account getAccount() {
		return account;
	}
	
	@Override
	public String toString() {
		return "Klient: " + person.getName() + " stan konta " + account.getBalance();
	}
}
