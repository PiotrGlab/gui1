package zad1;

public class Account {
	
	private double balance;
	private static double interest;
	
	public Account(){
		this.balance = 0;
	}
	
	public void deposit(double deposit) {
		if (deposit >= 0){
			balance += deposit;
		}
	}
	
	public void transfer(Account account, double transfer){
		if (transfer <= balance){
			balance -= transfer;
			account.balance += transfer;
		}
	}
	
	public void withdraw(double withdraw) {
		if (withdraw >= 0 && withdraw <= balance){
			balance -= withdraw;
		}
	}
	
	public static void setInterestRate(double percent) {
		if (percent >= 0){
			interest = percent/100;
		}
	}
	
	public void addInterest() {
		balance += balance * interest;
	}
	
	public double getBalance(){
		return balance;
	}

}
