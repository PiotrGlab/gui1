/**
 *
 *  @author Głąb Piotr S15601
 *
 */

package zad4;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.*;

public class CountWords {
	private String wholeFile;
	private String path;

	public CountWords(String fname) {
		path = fname;
		try{
			wholeFile = new String(Files.readAllBytes(Paths.get(path)));
		}catch(Exception ex){
			System.out.println("Brak pliku lub niewlasciwe dane");
		}
	}

	public ArrayList<String> getResult() {
		wholeFile += " ";
		ArrayList<String> stringsArray = new ArrayList<>();
		HashMap<String, Integer> result = new HashMap<String, Integer>();
		ArrayList<String> stringsWithoutIntArray = new ArrayList<>();
		ArrayList<String> stringsWithIntArray = new ArrayList<>();
		
		Pattern p = Pattern.compile("([a-zA-Z]{1,})(\\s|\\p{Punct})");
		Matcher m = p.matcher(wholeFile);
		while (m.find()) {
			String stringShort = m.group(1);
			stringsArray.add(stringShort);
		}
		
		for (int i = 0; i < stringsArray.size(); i++){
			String tmpKey = stringsArray.get(i);
			if (!result.containsKey(tmpKey)){
				result.put(tmpKey, 1);
				stringsWithoutIntArray.add(tmpKey);
			} else {
				result.put(tmpKey, result.get(tmpKey) + 1);
			}
		}
		
		for (int i = 0; i < stringsWithoutIntArray.size(); i++) {
			String tmp = stringsWithoutIntArray.get(i) + " " + result.get(stringsWithoutIntArray.get(i));
			stringsWithIntArray.add(tmp);
		}
		
		return stringsWithIntArray;
	}
	

}  
