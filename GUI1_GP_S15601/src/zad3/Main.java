/**
 *
 *  @author Głąb Piotr S15601
 *
 */

package zad3;

import java.nio.file.*;
import java.util.*;
import java.util.regex.*;

public class Main {

	public static void main(String[] args) {
		String fname = System.getProperty("user.home") + "/tab.txt";            
		String wholeFile = null;
		ArrayList<String> numbersArray = new ArrayList<>();
		
		try{
			wholeFile = new String(Files.readAllBytes(Paths.get(fname)));
		}catch(Exception ex){
			System.out.print("***");
			System.exit(0);
		}
		
		wholeFile += " ";
		
		Pattern p = Pattern.compile("(-?)(\\d+|[^ ]+)\\s+");
		Matcher m = p.matcher(wholeFile);
		while (m.find()) {
			String numbersShort = m.group();
			numbersShort = numbersShort.replaceAll("\\s+","");
			numbersArray.add(numbersShort);
		}
		
		if (numbersArray.size() == 0) {
			System.out.print("***");
	    	System.exit(0);
		}
		
		int[] numbersArrayInt = new int[numbersArray.size()];
		
		int maxValue = Integer.MIN_VALUE;
		for (int i = 0; i < numbersArrayInt.length; i++) {
		    try {
		    	numbersArrayInt[i] = Integer.parseInt(numbersArray.get(i));
		    	if (maxValue < numbersArrayInt[i]){
		    		maxValue = numbersArrayInt[i];
		    	}
		    } catch (NumberFormatException nfe) {
		    	System.out.print("***");
		    	System.exit(0);
		    }
		}
		
		for (int i = 0; i < numbersArrayInt.length; i++) {
			System.out.print(numbersArrayInt[i] + " ");
		}
		System.out.println();
		
		System.out.println(maxValue);
		
		for (int i = 0; i < numbersArrayInt.length; i++) {
			if (numbersArrayInt[i] == maxValue){
				System.out.print(i + " ");
			}
		}
	}
}
