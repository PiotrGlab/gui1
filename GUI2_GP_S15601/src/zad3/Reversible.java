package zad3;

public abstract class Reversible {

	public abstract void reverse();
	public abstract void reverseAgain();

}
