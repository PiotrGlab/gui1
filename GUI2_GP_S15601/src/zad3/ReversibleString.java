package zad3;

public class ReversibleString extends Reversible {
	
	private String string;

	public ReversibleString(String string) {
		this.string = string;
	}
	
	@Override
	public void reverse() {
		string = new StringBuilder(string).reverse().toString();
	}
	
	@Override
	public void reverseAgain() {
		string = new StringBuilder(string).reverse().toString();
		String text = "Tekst ";
		string = text + string;
	}
	
	@Override
	public String toString() {
		return string;
	}

}
