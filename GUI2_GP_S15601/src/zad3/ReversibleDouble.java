package zad3;

public class ReversibleDouble extends Reversible {
	
	private double digit;

	public ReversibleDouble(double digit) {
		this.digit = digit;
	}

	@Override
	public void reverse() {
		digit = 1 / digit;
	}
	
	@Override
	public void reverseAgain() {
		digit = (1 / digit) + 10;
	}
	
	@Override
	public String toString() {
		return String.valueOf(digit);
	}
}
