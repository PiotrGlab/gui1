package zad1;

public class ChoryNaDyspepsje extends Pacjent {
	
	private String sickness = "dyspepsja";
	private String treatment = "węgiel";
	
	public ChoryNaDyspepsje(String name) {
		super(name);
	}

	@Override
	public String choroba() {
		return sickness;
	}

	@Override
	public String leczenie() {
		return treatment;
	}

}
