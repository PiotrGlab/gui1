package zad1;

public class ChoryNaNoge extends Pacjent {
	
	private String sickness = "noga";
	private String treatment = "gips";
	
	public ChoryNaNoge(String name) {
		super(name);
	}

	@Override
	public String choroba() {
		return sickness;
	}

	@Override
	public String leczenie() {
		return treatment;
	}

}
