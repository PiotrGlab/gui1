package zad1;

public class ChoryNaGlowe extends Pacjent {
	
	private String sickness = "głowa";
	private String treatment = "aspiryna";

	public ChoryNaGlowe(String name) {
		super(name);
	}

	@Override
	public String choroba() {
		return sickness;
	}

	@Override
	public String leczenie() {
		return treatment;
	}

}
