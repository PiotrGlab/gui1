package zad2;

public abstract class Flower {
	
	protected int ammount;
	
	protected Flower(int ammount){
		this.ammount = ammount;
	}
	
	public abstract String getType();
	public abstract String getColor();
	
}
