package zad2;

import java.util.HashMap;

public class PriceList {
	
	private static PriceList pl;
	private static HashMap<String, Double> plm;
	
	private PriceList(){
		plm = new HashMap<>();
	}

   public static PriceList getInstance() {
	   if(pl == null) {
		   pl = new PriceList();
	   }
	   return pl;
   }

   public void put(String string, double d) {
	   plm.put(string, d);
   }
   
   public double getPrice(Flower fl) {
	   if (plm.get(fl.getType()) == null)
		   return -1;
	   return plm.get(fl.getType());
   }
}
