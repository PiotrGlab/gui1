package zad2;

public class Peony extends Flower {

	public Peony(int ammount) {
		super(ammount);
	}

	@Override
	public String getType() {
		return "piwonia";
	}

	@Override
	public String getColor() {
		return "czerwony";
	}
}
