package zad2;

public class Freesia extends Flower {
	
	public Freesia(int ammount) {
		super(ammount);
	}

	@Override
	public String getType() {
		return "frezja";
	}

	@Override
	public String getColor() {
		return "żółty";
	}
}
