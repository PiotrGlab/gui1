package zad2;

public class Rose extends Flower {

	public Rose(int ammount) {
		super(ammount);
	}

	@Override
	public String getType() {
		return "róża";
	}

	@Override
	public String getColor() {
		return "czerwony";
	}
}
