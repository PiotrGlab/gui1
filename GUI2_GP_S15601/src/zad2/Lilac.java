package zad2;

public class Lilac extends Flower {

	public Lilac(int ammount) {
		super(ammount);
	}

	@Override
	public String getType() {
		return "bez";
	}

	@Override
	public String getColor() {
		return "biały";
	}
}
