package zad2;

import java.util.ArrayList;

public class BoxLikeObject {
	
	protected ArrayList<Flower> content;
	public String owner;

	protected BoxLikeObject() {
		content = new ArrayList<>();
	}
	
	public void putInside(Flower fl) {
		content.add(fl);
	}
	
	public void empty() {
		content = new ArrayList<>();
	}
	
	@Override
	public String toString() {
		String includes = " właściciel " + owner;
		if (content.size() == 0) {
			return includes + " -- pusto";
		}
		for (int i = 0; i < content.size(); i++){
			includes += "\n" 
					+ (content.get(i)).getType() 
					+ ", kolor: " 
					+ (content.get(i)).getColor() 
					+ ", ilość " 
					+ (content.get(i)).ammount
					+ ", cena "
					+ PriceList.getInstance().getPrice(content.get(i));
		}
		return includes;
	}

}
