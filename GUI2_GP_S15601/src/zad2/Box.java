package zad2;

public class Box extends BoxLikeObject{
	
	public Box(Customer owner){
		super();
		this.owner = owner.getName();
	}
	
	@Override
	public String toString() {
		return "Pudełko" + super.toString();
	}
}
