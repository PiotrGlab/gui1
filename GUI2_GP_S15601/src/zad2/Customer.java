package zad2;

public class Customer {
	
	private String name;
	private double money;
	private ShoppingCart customerCart;
	
	public Customer(String name, double ammount) {
		this.name = name;
		this.money = ammount;
		customerCart = new ShoppingCart();
	}
	
	public String getName() {
		return name;
	}
	
	public void get(Flower fl) {
		customerCart.owner = name;
		customerCart.putInside(fl);
	}

	public void pay() {
		
		for (int i = 0; i < customerCart.content.size(); i++) {
			Flower tmp = customerCart.content.get(i);
			int ammount = tmp.ammount;
			double cost = (PriceList.getInstance().getPrice(tmp)) * ammount;
			
			if (PriceList.getInstance().getPrice(tmp) == -1 || cost > money){
				customerCart.content.remove(tmp);
				i -= 1;
			}else if (cost >= 0){
				money -= cost;
			}
		}
	}

	public double getCash() {
		return money;
	}

	public void pack(Box pudelkoJanka) {
		for (int i = 0; i < customerCart.content.size(); i++) {
			Flower tmp = customerCart.content.get(i);
			pudelkoJanka.putInside(tmp);
		}
		customerCart.empty();
	}

	public ShoppingCart getShoppingCart() {
		return customerCart;
	}
}
