/**
 *
 *  @author Głąb Piotr S15601
 *
 */

package zad4;


public abstract class Spiewak {
	
	private String name;
	private static int flag;
	private int startingNumber;
	
	public Spiewak(String name) {
		this.name = name;
		flag += 1;
		this.startingNumber = flag;
	}
	
	public abstract String spiewaj();
	
	@Override
	public String toString() {
		return "(" + startingNumber + ") " + name + ": " + spiewaj();
	}
	
	public static Spiewak najglosniej(Spiewak[] sp) {
		int sum = 0;
		int index = 0;
		for(int i = 0; i < sp.length; i++) {
			int tmp = sp[i].spiewaj().replaceAll("[A-Z]", "").length();
			int numberOfBigLetters = sp[i].spiewaj().length() - tmp;
			if(numberOfBigLetters > sum){
				sum = numberOfBigLetters;
				index = i;
			}
		}
		return sp[index];
	}
	
}
