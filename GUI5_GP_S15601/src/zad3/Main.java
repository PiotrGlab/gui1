/**
 *
 *  @author Głąb Piotr S15601
 *
 */

package zad3;

import java.awt.*;
import javax.swing.*;

public class Main {

	public static void main(String[] args) {

		JFrame frame = new JFrame("Title");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(800, 222);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);

		JTextArea textArea = new JTextArea("Test", 2, 2);
		Font font = new Font("Dialog", Font.ITALIC | Font.BOLD, 14);
		textArea.setBackground(Color.blue);
		textArea.setForeground(Color.yellow);
		textArea.setFont(font);
		textArea.setLineWrap(true);
		
		JScrollPane jscrollp = new JScrollPane(textArea);
		
		frame.add(jscrollp);

		frame.setVisible(true);
	}
}
