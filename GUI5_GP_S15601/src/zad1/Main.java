/**
 *
 *  @author Głąb Piotr S15601
 *
 */

package zad1;

import javax.swing.*;

public class Main {

	public static void main(String[] args) {

		JFrame frame = new JFrame("Title");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(400, 422);
		frame.add(new Painter());
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
