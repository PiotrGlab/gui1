package zad1;

import java.awt.*;
import javax.swing.*;

public class Painter extends JPanel {
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.blue);
		g.drawLine(-1, -1, getWidth()-1, getHeight()-1);
		g.drawLine(getWidth()-1, 0, 0, getHeight()-1);
	}
}
