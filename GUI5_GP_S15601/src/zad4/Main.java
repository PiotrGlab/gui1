/**
 *
 *  @author Głąb Piotr S15601
 *
 */

package zad4;


import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

public class Main {

  public static void main(String[] args) {
	
	    final int CNUM = 5; // liczba komponentów w panelach
	
	    String lmNames[] = {
			"BorderLayout",
	        "Flow Layout",
	        "Flow (left aligned)",
	        "Flow (right aligned)",
	        "Grid Layout(1 wiersz)",
	        "Grid Layout(1 kolumna)",
	        "Grid Layout(3, 2)", 
	    	};
	
	    LayoutManager lm[] = {
			new BorderLayout(),
			new FlowLayout(),
			new FlowLayout(FlowLayout.LEFT),
			new FlowLayout(FlowLayout.RIGHT),
			new GridLayout(1, 0),
			new GridLayout(0, 1),
			new GridLayout(3, 2),
			};
	    
	    String buttonNames[] = {
			"Przycisk 1",
			"P 2", 
			"Większy przycisk numer 3", 
			"Przycisk 4", 
			"P5" 
	        };
	    
		HashMap<String, Integer> map = new HashMap<>();
		map.put("A", 1);
		map.put("B", 2);
		map.put("C", 3);
		map.put("D", 4);
		map.put("E", 5);
		map.put("F", 6);
		map.put("G", 7);
	
		String gborders[] = { "West", "North", "East", "South", "Center" };
	
		Color colors[] = { new Color(191, 225, 255), new Color(255, 255, 200), new Color(201, 245, 245),
				new Color(255, 255, 140), new Color(161, 224, 224), new Color(255, 255, 200), new Color(255, 255, 200)};
	
		JFrame frame = new JFrame("Layouts show");
		boolean ifNotProperInput = false;
		
		while (!ifNotProperInput) {
			String response = JOptionPane.showInputDialog("Enter one of the following: A-G");
			try {
				int i = map.get(response);
				JPanel p = new JPanel();
				p.setBackground(colors[i]);
				p.setBorder(BorderFactory.createTitledBorder(lmNames[i]));
				p.setLayout(lm[i]);
				for (int j = 0; j < CNUM; j++) {
					JButton b = new JButton(buttonNames[j]);
					p.add(b, gborders[j]);
				}
				frame.add(p);
				ifNotProperInput = true;
			} catch (NullPointerException npe) {
				JOptionPane.showMessageDialog(null, "Niepoprawny input");
			}
		}
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
}
