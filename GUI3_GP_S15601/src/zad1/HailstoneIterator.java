package zad1;

import java.util.Iterator;

public class HailstoneIterator implements Iterator<Integer> {

	private int ini;

	public HailstoneIterator(int ini) {
		this.ini = ini;
	}
	@Override
	public boolean hasNext() {
		boolean result = this.ini > 0;
		return result;
	}

	@Override
	public Integer next() {
		if(this.hasNext()) {
            if (ini % 2 == 0){
            	int tmp = ini;
            	ini = ini / 2;
            	return tmp;
            } else if (ini % 2 != 0) {
        		if (ini == 1) {
        			ini = 0;
        			return 1;
        		}
            	int tmp = ini;
            	ini = (3 * ini) + 1;
            	return tmp;
            }
        }
		return -1;
	}

}
