/**
 *
 *  @author Głąb Piotr S15601
 *
 */

package zad2;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

public class Calc{
	Operation add = (nu1, nu2) -> {return nu1.add(nu2);};
	Operation subtract = (nu1, nu2) -> {return nu1.subtract(nu2);};
	Operation multiply = (nu1, nu2) -> {return nu1.multiply(nu2);};
	Operation divide = (nu1, nu2) -> {
		try {
			BigDecimal result = nu1.divide(nu2);
			return result;
		} catch(Exception ex) {
			return nu1.divide(nu2, 7, RoundingMode.HALF_UP);	
		}	
	};

	@SuppressWarnings("serial")
	HashMap<String,Operation> operations = new HashMap<String,Operation>(){{
		put("+", add);
		put("-", subtract);
		put("*", multiply);
		put("/", divide);}
	};
	
	public String doCalc(String input) {
		String[] tmp = input.split("\\s+");
		Double nu1tmp = Double.parseDouble(tmp[0]);
		BigDecimal nu1 = BigDecimal.valueOf(nu1tmp);
		String op = tmp[1].substring(0, 1);
		Double nu2tmp = Double.parseDouble(tmp[2]);
		BigDecimal nu2 = BigDecimal.valueOf(nu2tmp);
		BigDecimal result = Calc.calculate(nu1, nu2, operations.get(op));
		return result.toString();
	}
		
	public static BigDecimal calculate(BigDecimal nu1, BigDecimal nu2, Operation o) {
	      return o.calculate(nu1, nu2);
	}
}
		
	  
