package zad2;

import java.math.BigDecimal;

public interface Operation {

	public BigDecimal calculate(BigDecimal nu1, BigDecimal nu2);
}
